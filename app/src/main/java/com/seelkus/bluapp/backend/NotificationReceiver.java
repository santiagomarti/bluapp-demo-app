package com.seelkus.bluapp.backend;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.seelkus.bluapp.R;
import com.seelkus.bluappandroidsdk.Advert;
import com.seelkus.bluappandroidsdk.ImageViewActivity;
import com.seelkus.bluapp.frontend.AdvertsActivity;


public class NotificationReceiver extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_notification_receiver);
		Intent intent = null;
		if(getIntent().getBooleanExtra("fromnotification", false)){
		switch(getIntent().getExtras().getInt("adtype")){
				case Advert.HTML_CONTENT:
					intent = new Intent(this, AdvertsActivity.class);
					break;
				case Advert.IMAGE:
					intent = new Intent(this, ImageViewActivity.class);
					intent.putExtra("url", getIntent().getStringExtra("url"));
					break;
				case Advert.URL:
					intent = new Intent(Intent.ACTION_VIEW);
					intent.setData(Uri.parse(getIntent().getStringExtra("url")));
					break;
				case Advert.VIDEO:
					intent = new Intent(Intent.ACTION_VIEW);
					intent.setData(Uri.parse(getIntent().getStringExtra("url")));
					break;
				default:
					break;
			}
		} else {}
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		this.startActivity(intent);
	
	}
	
	@Override
	protected void onResume() {
		super.onResume();
	}
}
