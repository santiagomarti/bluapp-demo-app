package com.seelkus.bluapp.backend;

import java.util.HashMap;

import android.app.Application;
import android.graphics.Bitmap;
import android.util.SparseArray;

import com.seelkus.bluapp.R;
import com.seelkus.bluappandroidsdk.Advert;
import com.seelkus.bluappandroidsdk.BABeacon;
import com.seelkus.bluappandroidsdk.BeaconsStateListener;
import com.seelkus.bluappandroidsdk.BluManager;
import com.seelkus.bluappandroidsdk.Company;
import com.seelkus.bluapp.frontend.AdvertsActivity;


public class BluappApp extends Application implements BeaconsStateListener {
	
	public static SparseArray<Company> companies;
	public static SparseArray<Advert> adverts;
	public static SparseArray<BABeacon> onRangeBeacons;
	private static Application application;
	public static HashMap<Integer, Bitmap> bitmaps;
	
	@Override
	public void onCreate() {
		super.onCreate();
		application = this;
		bitmaps = new HashMap<>();
		companies = new SparseArray<>();
		adverts = new SparseArray<>();
		onRangeBeacons = new SparseArray<>();
	}

	@Override
	public void onRangedBeacon(BABeacon param) {
        onRangeBeacons.append(param.getId(), param);
	}

	@Override
	public void onUnrangedBeacon(BABeacon param) {
		for(int i = 0 ; i < onRangeBeacons.size() ; i++){
			BABeacon b = onRangeBeacons.valueAt(i);
			if(b.getId() == param.getId()) 
				onRangeBeacons.removeAt(i);
		}
	}
}