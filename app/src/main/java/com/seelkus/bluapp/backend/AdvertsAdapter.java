package com.seelkus.bluapp.backend;

import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.seelkus.bluappandroidsdk.Advert;


import com.seelkus.bluapp.R;
import com.seelkus.bluappandroidsdk.Advert;

import java.util.LinkedList;

public class AdvertsAdapter extends BaseAdapter implements ListAdapter {

	private Context context;
	public static LinkedList<Advert> advertsList;
	private Activity activity;
	private LayoutInflater inflater;
	private Display display;
	private Point size = new Point();
	public static com.seelkus.bluapp.backend.ImageLoader imageLoader;
	
	public AdvertsAdapter(Context c, LinkedList<Advert> advertsList, Activity activity){
		context = c;
		this.advertsList = advertsList;
		this.activity = activity;
	    inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    display = activity.getWindowManager().getDefaultDisplay();
	    imageLoader = new com.seelkus.bluapp.backend.ImageLoader(activity.getApplicationContext());
	}
	
	@Override
	public int getCount() {
		if(advertsList != null) return advertsList.size();
		else return 0;
	}

	@Override
	public Object getItem(int arg0) {
		return arg0;
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup arg2) {
		final Advert aux = advertsList.get(position);
		View lay = (View) convertView;  
		ViewHolder mHolder = null;
		if (convertView == null){
			lay = (View) inflater.inflate(R.layout.advert_button, null);
			mHolder = new ViewHolder();
			mHolder.image = (ImageView) lay.findViewById(R.id.advert_button);
			mHolder.title = (TextView) lay.findViewById(R.id.advert_title);
			lay.setTag(mHolder);
			display.getSize(size);
		} else {
			mHolder = (ViewHolder) lay.getTag();
		}	

		mHolder.image.setId(position);
		mHolder.title.setId(position);
		mHolder.AD_TYPE = aux.AD_TYPE;
		mHolder.position = position;
		mHolder.coverUrl = "http://ec2-54-233-70-75.sa-east-1.compute.amazonaws.com/covers/" + aux.getCover();
		mHolder.id = aux.getId();
		mHolder.title.setText(aux.getTitle());
		
		ImageView image = mHolder.image;
        imageLoader.DisplayImage(mHolder.coverUrl, image);
		return lay; 
	}

	private class ViewHolder {
		private ImageView image;
		private TextView title;
		private int id;
		private String coverUrl;
		private int position;
		private int AD_TYPE;
	}
}
