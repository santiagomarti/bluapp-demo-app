package com.seelkus.bluapp.frontend;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.support.v7.app.AppCompatActivity;

import com.google.gson.JsonParser;
import com.seelkus.bluapp.R;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class Login extends AppCompatActivity {

    private  EditText mail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mail = (EditText) findViewById(R.id.mail);
        Button btn = (Button) findViewById(R.id.btn_login);
        if(mailOnSharedPreferences()){
            Intent i = new Intent(Login.this, AdvertsActivity.class);
            startActivity(i);
        }
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isMailValid(mail.getText().toString()))
                    new DownloadImageTask().execute(mail.getText().toString());
            }
        });
    }

    private boolean mailOnSharedPreferences() {
        return !getSharedPreferences("bluapp", Context.MODE_PRIVATE).getString("mail", "").equals("");
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Boolean> {
        AlertDialog.Builder dialog = new AlertDialog.Builder(Login.this, R.style.MyAlertDialogStyle);
        AlertDialog alert = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            alert = dialog.setTitle("Bluapp").setMessage("Verificando...").setCancelable(false).create();
            alert.show();
        }

        protected Boolean doInBackground(String... urls) {
            try {
            String mail = urls[0];
            HttpParams httpParams = new BasicHttpParams();
            HttpConnectionParams.setConnectionTimeout(httpParams, 30000);
            HttpConnectionParams.setSoTimeout(httpParams, 30000);
            DefaultHttpClient client = new DefaultHttpClient(httpParams);

            HttpPost request = new HttpPost("http://54.233.70.75:8001/ws/call/");
            request.setHeader("User-Agent", "Dalvik/1.4.0 (Linux; U; Android 2.3.5)");

            JSONObject parameters = new JSONObject();
            parameters.put("customerid", mail);

            JSONObject query = new JSONObject();
            query.put("id", String.valueOf(System.currentTimeMillis()));
            query.put("name", "isUserRegistered");
            query.put("args", parameters);
            query.put("session", "");

            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
            nameValuePairs.add(new BasicNameValuePair("query", query.toString()));
            request.setEntity(new UrlEncodedFormEntity(nameValuePairs));

            HttpResponse response = client.execute(request);
            String line = "";
            StringBuilder sb = new StringBuilder();
                BufferedReader reader = new BufferedReader(new InputStreamReader(
                        response.getEntity().getContent()), 65728);
                line = null;

                while ((line = reader.readLine()) != null) {
                    sb.append(line);
                }
                reader.close();
                line = sb.toString();
                JsonParser parser = new JsonParser();
                return parser.parse(line).getAsJsonObject().get("response").getAsString().equals("true");
            } catch (IOException e) {
                e.printStackTrace();
            } catch (Exception e) {
                e.printStackTrace();
            }
            return false;
        }

        protected void onPostExecute(Boolean isClient) {
            if(isClient){
                SharedPreferences sharedPref = getSharedPreferences("bluapp", Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("mail", mail.getText().toString());
                editor.commit();
                Intent i = new Intent(Login.this, AdvertsActivity.class);
                startActivity(i);

            } else {
                Toast.makeText(Login.this, "Usuario inválido", Toast.LENGTH_LONG).show();
            }
            alert.dismiss();
        }
    }

    private boolean isMailValid(String mail){
        return android.util.Patterns.EMAIL_ADDRESS.matcher(mail).matches();
    }
}
