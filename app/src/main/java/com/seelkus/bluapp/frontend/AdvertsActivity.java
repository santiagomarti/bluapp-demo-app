package com.seelkus.bluapp.frontend;

import java.util.LinkedList;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.etsy.android.grid.StaggeredGridView;
import com.seelkus.bluapp.R;
import com.seelkus.bluapp.backend.AdvertsAdapter;
import com.seelkus.bluapp.backend.BluappApp;
import com.seelkus.bluappandroidsdk.Advert;
import com.seelkus.bluappandroidsdk.BABeacon;
import com.seelkus.bluappandroidsdk.BeaconsStateListener;
import com.seelkus.bluappandroidsdk.BluManager;
import com.seelkus.bluappandroidsdk.ImageViewActivity;


public class AdvertsActivity extends AppCompatActivity implements BeaconsStateListener {

	public static StaggeredGridView grid;
	public static LinkedList<Advert> localAdverts;
	public static LinkedList<Advert> localAdvertsAux;
	private AdvertsActivity instance;
    private static View explanation, disclaimer;
    private ImageView logo;
	private View turn_bt;
    private Button bt_btn;
    private int SELECTED_ADVERT_POSITION = 0;
    public static BluManager manager;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_adverts);
		localAdverts = new LinkedList<>();
		localAdvertsAux = new LinkedList<>();

        manager = new BluManager(getApplication());
        manager.start();

		instance = this;
		for(int i = 0 ; i < BluappApp.onRangeBeacons.size() ; i++){
			BABeacon aux = BluappApp.onRangeBeacons.valueAt(i);
			for(int j = 0 ; j < aux.getAds().size() ; j++){
				Advert aux2 = aux.getAds().valueAt(j);
				localAdverts.add(aux2);
				localAdvertsAux.add(aux2);
			}
		}

		grid = (StaggeredGridView) findViewById(R.id.activity_adverts_gridlayout);
		grid.setScrollingCacheEnabled(true);
		grid.setAnimationCacheEnabled(false);
        grid.setAdapter(new AdvertsAdapter(getBaseContext(), localAdvertsAux, this));
		grid.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                int AD_TYPE = localAdvertsAux.get(position).AD_TYPE;
                SELECTED_ADVERT_POSITION = position;
                switch (AD_TYPE) {
                    case Advert.HTML_CONTENT:
                        break;
                    case Advert.IMAGE:
                        Intent i1 = new Intent(AdvertsActivity.this, ImageViewActivity.class);
                        i1.putExtra("url", localAdvertsAux.get(SELECTED_ADVERT_POSITION).getUrl());
                        startActivity(i1);
                        break;
                    case Advert.URL:
                        Intent i2 = new Intent(Intent.ACTION_VIEW);
                        i2.setData(Uri.parse(localAdvertsAux.get(SELECTED_ADVERT_POSITION).getUrl()));
                        startActivity(i2);
                        break;
                    case Advert.VIDEO:
                        Intent i3 = new Intent(Intent.ACTION_VIEW);
                        i3.setData(Uri.parse(localAdvertsAux.get(SELECTED_ADVERT_POSITION).getUrl()));
                        startActivity(i3);
                        break;
                    default:
                        break;
                }
            }
        });
        manager.registerBeaconStateListener(this);
		IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
		instance.getApplication().registerReceiver(mReceiver, filter);

		explanation = findViewById(R.id.adverts_explanation);
		disclaimer = findViewById(R.id.adverts_disclaimer);
		turn_bt = findViewById(R.id.activate_btn_card);
        logo = (ImageView) findViewById(R.id.bpLog);
        bt_btn = (Button) findViewById(R.id.activate_bt);

		final BluetoothAdapter bluetooth = BluetoothAdapter.getDefaultAdapter();
		if(bluetooth == null){
			Toast.makeText(getApplicationContext(), "Bluetooth not supported.", Toast.LENGTH_LONG);
		} else {
			if(bluetooth.isEnabled() || bluetooth.isDiscovering()){
				explanation.setVisibility(View.GONE);
				turn_bt.setVisibility(View.GONE);
				if(localAdvertsAux.size() > 0)
					disclaimer.setVisibility(View.GONE);
				logo.setVisibility(View.GONE);
			} else {
				disclaimer.setVisibility(View.GONE);
				logo.setVisibility(View.GONE);
				if(localAdvertsAux.size() > 0){
					explanation.setVisibility(View.GONE);
					turn_bt.setVisibility(View.GONE);
				}
                bt_btn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        bluetooth.enable();
                        explanation.setVisibility(View.GONE);
                        turn_bt.setVisibility(View.GONE);
                    }
                });
			}
		}

		logo.setVisibility(View.VISIBLE);
	}
	
	private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			final String action = intent.getAction();

			if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
				final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
				switch (state) {
					case BluetoothAdapter.STATE_OFF:
						break;
					case BluetoothAdapter.STATE_TURNING_OFF:
						break;
					case BluetoothAdapter.STATE_ON:
						break;
					case BluetoothAdapter.STATE_TURNING_ON:
						explanation.setVisibility(View.GONE);
						turn_bt.setVisibility(View.GONE);
						if(localAdverts.size() == 0){
							disclaimer.setVisibility(View.VISIBLE);
							logo.setVisibility(View.VISIBLE);
						}
						break;
				}
			}
		}
	};
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		instance = null;
	}

	@Override
	public void onRangedBeacon(BABeacon aux) {
		if(this.isDestroyed() == false){
			instance.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					explanation.setVisibility(View.GONE);
					turn_bt.setVisibility(View.GONE);
					disclaimer.setVisibility(View.GONE);
                    logo.setVisibility(View.GONE);
				}
			});
		}

		int initSize = localAdverts.size();
		if(aux != null)
			for(int j = 0 ; j < aux.getAds().size() ; j++){
				Advert aux2 = aux.getAds().valueAt(j);
				boolean isPresent = false;
				for(int i = 0 ; i < localAdverts.size() ; i++){
					Advert aux3 = localAdverts.get(i);
					if(aux3.getId() == aux2.getId()) {
						isPresent = true;
						break;
					}
				}
				if(isPresent == false) {
					localAdverts.add(aux2);
					localAdvertsAux.add(aux2);
				}
			}

		int finSize = localAdverts.size();
        if(initSize != finSize)
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    ((AdvertsAdapter) grid.getAdapter()).notifyDataSetChanged();
                }
            });
	}
	
	@Override
	public void onResume() {
		super.onResume();
		((AdvertsAdapter) grid.getAdapter()).notifyDataSetChanged();
	}
	
	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {
		switch (item.getItemId()) {
		default:
			break;
		}
		return true;
	}

	
	@Override
	public void onUnrangedBeacon(BABeacon param) {}

}