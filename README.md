Our Android SDK is built over the AltBeacon specification and SDK. It works as a background sticky service, wich automatically detects Bluapp's iBeacons and shows their related content and registers important analitics. If you have any questions please write at santiago@bluapp.cl. Here, you can also find our testing app. The SDK is contained within the `android-sdk` module.

#Step 1:
Include the JCenter repository in your project's `build.gradle` file:
```
repositories {
        jcenter()
}
```

#Step 2
Insert the following dependency in your app's `build.gradle` file:
```
dependencies {
    compile 'com.seelkus:android-sdk:1.2'
}
```
#Step 3
Add the following metadata to your app's `Manifest.xml` file:
```
<meta-data android:name="bluapp-client" android:value="client@mail.com" />
<meta-data android:name="bluapp-company" android:value="YourCompany" />
<meta-data android:name="bluapp-smallicon" android:value="StatusBarIcon" />
<meta-data android:name="bluapp-largeicon" android:value="LargeIcon" />
```
Please note that you must enter your client email as our team provides it to you. The icon fields must be filled with pictures wich MUST be inside the drawable folder. Otherwise, notifications won't appear. Large icon is the main notification icon and the small one is the status bar icon.

#Step 4

Finally, initialize our service anywhere in your code:
```
BluManager manager = new BluManager(getApplication());
manager.start();
```