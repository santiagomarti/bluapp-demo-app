package com.seelkus.bluappandroidsdk;

public class Local {

	private Company company;
	private int id, company_id, active;
	private String name, cover, description;
	public Company getCompany() {
		return company;
	}
	protected void setCompany(Company company) {
		this.company = company;
	}
	public int getId() {
		return id;
	}
	protected void setId(int id) {
		this.id = id;
	}
	public int getCompany_id() {
		return company_id;
	}
	protected void setCompany_id(int company_id) {
		this.company_id = company_id;
	}
	public int getActive() {
		return active;
	}
	protected void setActive(int active) {
		this.active = active;
	}
	public String getName() {
		return name;
	}
	protected void setName(String name) {
		this.name = name;
	}
	public String getCover() {
		return cover;
	}
	protected void setCover(String cover) {
		this.cover = cover;
	}
	public String getDescription() {
		return description;
	}
	protected void setDescription(String description) {
		this.description = description;
	}

	
}
