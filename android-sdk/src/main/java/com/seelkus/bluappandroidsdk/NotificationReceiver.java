package com.seelkus.bluappandroidsdk;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.SparseArray;

public class NotificationReceiver extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		BluManager.notificationTitles = new SparseArray<String>();
		setContentView(R.layout.activity_notification_receiver);
		Intent intent = null;
		if (getIntent().getBooleanExtra("enter", true))
			DataTracker.onNotificationOpenedEnter(
					getIntent().getIntExtra("id", 0), getIntent()
							.getStringExtra("proxuuid"), getIntent()
							.getLongExtra("major", 0), getIntent()
							.getLongExtra("minor", 0), getDeviceId());
		if (getIntent().getBooleanExtra("fromnotification", true)) {
			switch (getIntent().getExtras().getInt("adtype")) {
			case Advert.HTML_CONTENT:
				// intent = new Intent(this, Home.class);
				intent = null;
				break;
			case Advert.IMAGE:
				intent = new Intent(this, ImageViewActivity.class);
				intent.putExtra("url", getIntent().getStringExtra("url"));
				break;
			case Advert.URL:
				intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse(getIntent().getStringExtra("url")));
				break;
			case Advert.VIDEO:
				intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse(getIntent().getStringExtra("url")));
				break;
			default:
				break;
			}
		} else {
		}
		intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
		this.startActivity(intent);
	}

	public String getDeviceId() {
		TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
		return tm.getDeviceId();
	}

	@Override
	protected void onResume() {
		super.onResume();
		Intent intent = null;
		// intent = new Intent(this, Home.class);
	}
}
