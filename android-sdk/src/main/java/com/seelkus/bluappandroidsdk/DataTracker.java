package com.seelkus.bluappandroidsdk;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import android.util.Log;
import android.util.SparseArray;
import android.util.SparseBooleanArray;

public class DataTracker {

	private static SparseBooleanArray enteredInfo = new SparseBooleanArray();
    private static SparseArray<BABeacon> lastRoundBeacons = new SparseArray<>();

    protected static void trackingService(){
        Log.e("Bluapp", "Tracking Service");
        Set<BABeacon> olds = new HashSet<>(), news = new HashSet<>(), newToRange = new HashSet<>(), outOfRange = new HashSet<>();
        for(int i = 0; i < BluManager.onRangeBeacons.size(); i++) {
            BABeacon aux = BluManager.onRangeBeacons.valueAt(i);
            news.add(aux);
            outOfRange.add(aux);
            newToRange.add(aux);
        }

        for(int i = 0; i < lastRoundBeacons.size(); i++)
            olds.add(lastRoundBeacons.valueAt(i));

        outOfRange.addAll(olds);
        outOfRange.removeAll(news);
        newToRange.addAll(olds);
        newToRange.removeAll(olds);

        for(BABeacon b : outOfRange) {
            Log.e("Bluapp", "Exit range");
            onExitedRegion(b.getId(), b, BluManager.getDeviceId());
        }

        for(BABeacon b : newToRange) {
            onEnteredRegion(b.getId(), b, BluManager.getDeviceId());
            Log.e("Bluapp", "Enter range");
        }

        lastRoundBeacons = new SparseArray<>();
        for(int i = 0; i < BluManager.onRangeBeacons.size(); i++)
            lastRoundBeacons.append(BluManager.onRangeBeacons.valueAt(i).getId(), BluManager.onRangeBeacons.valueAt(i));

    }

    protected static void onBluetoothOff(){
        for(int i = 0; i < BluManager.onRangeBeacons.size(); i++) {
            BABeacon aux = BluManager.onRangeBeacons.valueAt(i);
            onExitedRegion(aux.getId(), aux, BluManager.getDeviceId());
        }
        Log.e("Bluapp", "BT off, all out of range");
    }

	public static void onNotificationSentEnter(final int id, final BABeacon beacon,
			final String deviceId) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Log.e("Bluapp", "Sent notification on enter, add id: " + id);
				try {
					WebServiceClient.onPushSentEnter(id, beacon.getProximityUUID(), beacon.getMajor(), beacon.getMinor(), deviceId);
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (JSONException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}).start();
	}


	public static void onNotificationOpenedEnter(final int id,
			final String uuid, final long major, final long minor,
			final String deviceId) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				Log.e("Bluapp", "Opened notification, add id: " + id);
				try {
					WebServiceClient.onPushOpenedEnter(id, uuid, major, minor,
                            deviceId);
				} catch (ClientProtocolException e) {
					e.printStackTrace();
				} catch (JSONException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}).start();
	}

	public static void onEnteredRegion(final int beaconId,
			final BABeacon beacon, final String deviceId) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				if (enteredInfo.get(beaconId, false) == false) {
					enteredInfo.put(beaconId, true);
					try {
						WebServiceClient.onEnteredRegion(beacon.getProximity(),
                                beacon.getProximityUUID(), beacon.getMajor(),
                                beacon.getMinor(), deviceId);
					} catch (ClientProtocolException e) {
						e.printStackTrace();
					} catch (JSONException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}).start();
	}

	public static void onExitedRegion(final int beaconId,
			final BABeacon beacon, final String deviceId) {
		new Thread(new Runnable() {

			@Override
			public void run() {
				if (enteredInfo.get(beaconId) == true) {
					enteredInfo.put(beaconId, false);
					try {
						WebServiceClient.onExitedRegion(beacon.getProximity(),
                                beacon.getProximityUUID(), beacon.getMajor(),
                                beacon.getMinor(), deviceId);
					} catch (ClientProtocolException e) {
						e.printStackTrace();
					} catch (JSONException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}).start();
	}
}
