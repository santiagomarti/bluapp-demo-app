package com.seelkus.bluappandroidsdk;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Santiago Martí on 8/10/15.
 */
public class DBManager extends SQLiteOpenHelper {

    private static String DATABASE_NAME = "bluapp_db";
    private static int DATABASE_VERSION = 1;

    private static String table_campaigns = "campaigns";
    private static String id = "id";
    private static String created_at = "created_at";
    private static String last_sent_on_enter = "last_sent_on_enter";
    private static String last_sent_on_exit = "last_sent_on_exit";
    private static String server_modified_at = "server_mod"; //Campo que valida si el beacon ha sido modificado en el servidor, para recargarlo
    private static String campaign_id = "cid";
    private static String first_time_seen = "ftseen";

    private static String CAMPAIGNS_TABLE_CREATOR = "create table " + table_campaigns + " ( " + id + " integer primary key, " +
            campaign_id + " integer not null unique, " + last_sent_on_exit + " integer default 0, " + last_sent_on_enter + " integer default 0, "
            + created_at + " datetime, " + server_modified_at + " datetime, " + first_time_seen + " integer default 0)";


    public DBManager(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CAMPAIGNS_TABLE_CREATOR);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public boolean advertExistsOnDB(int serverId){
        SQLiteDatabase db = this.getReadableDatabase();

        if(db != null){
            String selectQuery = "SELECT  * FROM " + table_campaigns + " WHERE "
                    + campaign_id + " = " + serverId;

            Cursor c = db.rawQuery(selectQuery, null);

            if (c != null)
                return c.moveToFirst();
            else return false;
        }
        return false;
    }

    public void createAdvert(Advert ad) {
        SQLiteDatabase db = this.getWritableDatabase();

        if(db != null){
            ContentValues values = new ContentValues();
            values.put(campaign_id, ad.getId());
            values.put(last_sent_on_enter, ad.lastTimeShowed);
            values.put(last_sent_on_exit, ad.lastTimeShowedOnExit);
            values.put(created_at, getDateTime());
            values.put(first_time_seen, ad.getFirstTimeSeen());
            db.insert(table_campaigns, null, values);
            db.close();
        }
    }

    public Advert getAdvert (int serverId){
        SQLiteDatabase db = this.getReadableDatabase();

        if(db != null){
            String selectQuery = "SELECT  * FROM " + table_campaigns + " WHERE "
                    + campaign_id + " = " + serverId;
            Cursor c = db.rawQuery(selectQuery, null);

            if (c != null)
                c.moveToFirst();

            Advert ad = new Advert();
            ad.setLastTimeShowed((c.getLong(c.getColumnIndex(last_sent_on_enter))));
            ad.setLastTimeShowedOnExit(c.getLong(c.getColumnIndex(last_sent_on_exit)));
            ad.setFirstTimeSeen(c.getLong(c.getColumnIndex(first_time_seen)));
            db.close();
            return ad;
        }
        return null;
    }

    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "yyyy-MM-dd HH:mm:ss", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    protected void updateAdvert(Advert ad) {
        SQLiteDatabase db = this.getWritableDatabase();

        if(db != null) {
            ContentValues values = new ContentValues();
            values.put(last_sent_on_exit, ad.getLastTimeShowedOnExit());
            values.put(last_sent_on_enter, ad.getLastTimeShowed());
            values.put(first_time_seen, ad.getFirstTimeSeen());

            // updating row
            db.update(table_campaigns, values, campaign_id + " = ?",
                    new String[] { String.valueOf(ad.getId()) });
            db.close();
        }
    }
}
