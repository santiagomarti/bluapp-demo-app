package com.seelkus.bluappandroidsdk;

public class BluAppNotInitializedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public BluAppNotInitializedException(String message)
	{
		super(message);
	}

}
