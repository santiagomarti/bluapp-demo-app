package com.seelkus.bluappandroidsdk;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

public class WebServiceClient {

	private static String baseUrl = "http://54.233.70.75:8001/ws/call/";
	private static ErrorCatcher catcher;

	public static synchronized LinkedList<BABeacon> getClientBeacons()
			throws ClientProtocolException, IOException, JSONException {
		LinkedList<BABeacon> ret = new LinkedList<BABeacon>();
		HttpParams httpParams = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParams, 30000);
		HttpConnectionParams.setSoTimeout(httpParams, 30000);
		DefaultHttpClient client = new DefaultHttpClient(httpParams);

		HttpPost request = new HttpPost(baseUrl);
		request.setHeader("User-Agent",
				"Dalvik/1.4.0 (Linux; U; Android 2.3.5)");

		JSONObject parameters = new JSONObject();
		parameters.put("customerid",
				BluManager.getClientEmail() == null ? "noEmailRegistered"
						: BluManager.getClientEmail());

		JSONObject query = new JSONObject();
		query.put("id", String.valueOf(System.currentTimeMillis()));
		query.put("name", "getUIDS");
		query.put("args", parameters);
		query.put("session", "");

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("query", query.toString()));
		request.setEntity(new UrlEncodedFormEntity(nameValuePairs));

		HttpResponse response = client.execute(request);
		StringBuilder sb = new StringBuilder();
		String result = "";
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()), 65728);
			result = null;
			while ((result = reader.readLine()) != null) {
				sb.append(result);
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		result = sb.toString();
		JSONParser parser = new JSONParser();
		return parser.parseBeacons(result);
	}

	public static synchronized BABeacon getBeacon(String uuid, int major,
			int minor) throws JSONException, ClientProtocolException,
			IOException {
		HttpParams httpParams = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParams, 30000);
		HttpConnectionParams.setSoTimeout(httpParams, 30000);
		DefaultHttpClient client = new DefaultHttpClient(httpParams);

		HttpPost request = new HttpPost(baseUrl);
		request.setHeader("User-Agent",
				"Dalvik/1.4.0 (Linux; U; Android 2.3.5)");

		JSONObject parameters = new JSONObject();
		parameters.put("uuid", uuid);
		parameters.put("major", major + "");
		parameters.put("minor", minor + "");

		JSONObject query = new JSONObject();
		query.put("id", String.valueOf(System.currentTimeMillis()));
		query.put("name", "getbeacon");
		query.put("args", parameters);
		query.put("session", "");

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("query", query.toString()));
		request.setEntity(new UrlEncodedFormEntity(nameValuePairs));

		HttpResponse response = client.execute(request);
		String line = "";
		StringBuilder sb = new StringBuilder();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()), 65728);
			line = null;

			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		line = sb.toString();
		JSONParser parserSeelkus = new JSONParser();
		JsonParser parserGson = new JsonParser();
		JsonElement element = parserGson.parse(line);
		return parserSeelkus.parseBeacon(element, true, true);
	}

	public static synchronized LinkedList<Advert> getCampaign(int beaconId) throws JSONException, ClientProtocolException, IOException {
		HttpParams httpParams = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParams, 30000);
		HttpConnectionParams.setSoTimeout(httpParams, 30000);
		DefaultHttpClient client = new DefaultHttpClient(httpParams);

		HttpPost request = new HttpPost(baseUrl);
		request.setHeader("User-Agent",
				"Dalvik/1.4.0 (Linux; U; Android 2.3.5)");

		JSONObject parameters = new JSONObject();
		parameters.put("idbeacon", beaconId);

		JSONObject query = new JSONObject();
		query.put("id", String.valueOf(System.currentTimeMillis()));
		query.put("name", "getcampain");
		query.put("args", parameters);
		query.put("session", "");

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("query", query.toString()));
		request.setEntity(new UrlEncodedFormEntity(nameValuePairs));

		HttpResponse response = client.execute(request);
		String line = "";
		StringBuilder sb = new StringBuilder();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()), 65728);
			line = null;

			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		line = sb.toString();
		JSONParser parserSeelkus = new JSONParser();
		JsonParser parserGson = new JsonParser();
		JsonElement element = parserGson.parse(line);
		return parserSeelkus.parseAdverts(element);
	}
	
	public static synchronized void onExitedRegion(String proximity, String proximityId, long major, long minor, String deviceId) throws JSONException, ClientProtocolException, IOException{
		HttpParams httpParams = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParams, 30000);
		HttpConnectionParams.setSoTimeout(httpParams, 30000);
		DefaultHttpClient client = new DefaultHttpClient(httpParams);

		HttpPost request = new HttpPost(baseUrl);
		request.setHeader("User-Agent",
				"Dalvik/1.4.0 (Linux; U; Android 2.3.5)");

		JSONObject parameters = new JSONObject();
		parameters.put("proximity", proximity);
		parameters.put("proximityuuid", proximityId);
		parameters.put("major", major);
		parameters.put("minor", minor);
		parameters.put("deviceid", deviceId);
		parameters.put("enter", false);

		JSONObject query = new JSONObject();
		query.put("id", String.valueOf(System.currentTimeMillis()));
		query.put("name", "trackingdevice_range");
		query.put("args", parameters);
		query.put("session", "");

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("query", query.toString()));
		request.setEntity(new UrlEncodedFormEntity(nameValuePairs));

		HttpResponse response = client.execute(request);
		String line = "";
		StringBuilder sb = new StringBuilder();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()), 65728);
			line = null;

			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		line = sb.toString();
	}
	
	public static synchronized void onEnteredRegion(String proximity, String proximityId, long major, long minor, String deviceId) throws JSONException, ClientProtocolException, IOException{
		HttpParams httpParams = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParams, 30000);
		HttpConnectionParams.setSoTimeout(httpParams, 30000);
		DefaultHttpClient client = new DefaultHttpClient(httpParams);

		HttpPost request = new HttpPost(baseUrl);
		request.setHeader("User-Agent",
				"Dalvik/1.4.0 (Linux; U; Android 2.3.5)");

		JSONObject parameters = new JSONObject();
		parameters.put("proximity", proximity);
		parameters.put("proximityuuid", proximityId);
		parameters.put("major", major);
		parameters.put("minor", minor);
		parameters.put("deviceid", deviceId);
		parameters.put("enter", true);

		JSONObject query = new JSONObject();
		query.put("id", String.valueOf(System.currentTimeMillis()));
		query.put("name", "trackingdevice_range");
		query.put("args", parameters);
		query.put("session", "");

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("query", query.toString()));
		request.setEntity(new UrlEncodedFormEntity(nameValuePairs));

		HttpResponse response = client.execute(request);
		String line = "";
		StringBuilder sb = new StringBuilder();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()), 65728);
			line = null;

			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		line = sb.toString();
	}
	
	public static synchronized void onPushOpenedEnter(int campaignId, String proximityId, long major, long minor, String deviceId) throws JSONException, ClientProtocolException, IOException{
		HttpParams httpParams = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParams, 30000);
		HttpConnectionParams.setSoTimeout(httpParams, 30000);
		DefaultHttpClient client = new DefaultHttpClient(httpParams);

		HttpPost request = new HttpPost(baseUrl);
		request.setHeader("User-Agent",
				"Dalvik/1.4.0 (Linux; U; Android 2.3.5)");

		JSONObject parameters = new JSONObject();
		parameters.put("proximityuuid", proximityId);
		parameters.put("major", major);
		parameters.put("minor", minor);
		parameters.put("deviceid", deviceId);
		parameters.put("idcamp",campaignId);
		parameters.put("data", "");
		parameters.put("enter", true);

		JSONObject query = new JSONObject();
		query.put("id", String.valueOf(System.currentTimeMillis()));
		query.put("name", "trackingdevice_info");
		query.put("args", parameters);
		query.put("session", "");

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("query", query.toString()));
		request.setEntity(new UrlEncodedFormEntity(nameValuePairs));

		HttpResponse response = client.execute(request);
		String line = "";
		StringBuilder sb = new StringBuilder();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()), 65728);
			line = null;

			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		line = sb.toString();
	}
	
	public static synchronized void onPushOpenedExit(int campaignId, String proximityId, long major, long minor, String deviceId) throws JSONException, ClientProtocolException, IOException{
		HttpParams httpParams = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParams, 30000);
		HttpConnectionParams.setSoTimeout(httpParams, 30000);
		DefaultHttpClient client = new DefaultHttpClient(httpParams);

		HttpPost request = new HttpPost(baseUrl);
		request.setHeader("User-Agent",
				"Dalvik/1.4.0 (Linux; U; Android 2.3.5)");

		JSONObject parameters = new JSONObject();
		parameters.put("proximityuuid", proximityId);
		parameters.put("major", major);
		parameters.put("minor", minor);
		parameters.put("deviceid", deviceId);
		parameters.put("enter", false);
		parameters.put("data", "");
		parameters.put("idcamp",campaignId);
		
		JSONObject query = new JSONObject();
		query.put("id", String.valueOf(System.currentTimeMillis()));
		query.put("name", "trackingdevice_info");
		query.put("args", parameters);
		query.put("session", "");

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("query", query.toString()));
		request.setEntity(new UrlEncodedFormEntity(nameValuePairs));

		HttpResponse response = client.execute(request);
		String line = "";
		StringBuilder sb = new StringBuilder();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()), 65728);
			line = null;

			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		line = sb.toString();
	}
	
	public static synchronized void onPushSentEnter(int campaignId, String proximityId, long major, long minor, String deviceId) throws JSONException, ClientProtocolException, IOException{
		HttpParams httpParams = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParams, 30000);
		HttpConnectionParams.setSoTimeout(httpParams, 30000);
		DefaultHttpClient client = new DefaultHttpClient(httpParams);

		HttpPost request = new HttpPost(baseUrl);
		request.setHeader("User-Agent",
				"Dalvik/1.4.0 (Linux; U; Android 2.3.5)");

		JSONObject parameters = new JSONObject();
		parameters.put("proximityuuid", proximityId);
		parameters.put("major", major);
		parameters.put("minor", minor);
		parameters.put("deviceid", deviceId);
		parameters.put("enter", true);
		parameters.put("data", "");
		parameters.put("idcamp",campaignId);
		
		JSONObject query = new JSONObject();
		query.put("id", String.valueOf(System.currentTimeMillis()));
		query.put("name", "trackingdevice_push");
		query.put("args", parameters);
		query.put("session", "");

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("query", query.toString()));
		request.setEntity(new UrlEncodedFormEntity(nameValuePairs));

		HttpResponse response = client.execute(request);
		String line = "";
		StringBuilder sb = new StringBuilder();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()), 65728);
			line = null;

			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		line = sb.toString();
	}
	
	public static synchronized void onPushSentExit(int campaignId, String proximityId, long major, long minor, String deviceId) throws JSONException, ClientProtocolException, IOException{
		HttpParams httpParams = new BasicHttpParams();
		HttpConnectionParams.setConnectionTimeout(httpParams, 30000);
		HttpConnectionParams.setSoTimeout(httpParams, 30000);
		DefaultHttpClient client = new DefaultHttpClient(httpParams);

		HttpPost request = new HttpPost(baseUrl);
		request.setHeader("User-Agent",
				"Dalvik/1.4.0 (Linux; U; Android 2.3.5)");

		JSONObject parameters = new JSONObject();
		parameters.put("proximityuuid", proximityId);
		parameters.put("major", major);
		parameters.put("minor", minor);
		parameters.put("deviceid", deviceId);
		parameters.put("enter", false);
		parameters.put("data", "");
		parameters.put("idcamp",campaignId);
		
		JSONObject query = new JSONObject();
		query.put("id", String.valueOf(System.currentTimeMillis()));
		query.put("name", "trackingdevice_push");
		query.put("args", parameters);
		query.put("session", "");

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("query", query.toString()));
		request.setEntity(new UrlEncodedFormEntity(nameValuePairs));

		HttpResponse response = client.execute(request);
		String line = "";
		StringBuilder sb = new StringBuilder();
		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					response.getEntity().getContent()), 65728);
			line = null;

			while ((line = reader.readLine()) != null) {
				sb.append(line);
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		line = sb.toString();
	}
}
