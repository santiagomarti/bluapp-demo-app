package com.seelkus.bluappandroidsdk;

public class BluetoothNotEnabledException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public BluetoothNotEnabledException(String message)
	{
		super(message);
	}

}
