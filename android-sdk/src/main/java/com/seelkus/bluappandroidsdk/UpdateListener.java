package com.seelkus.bluappandroidsdk;

public interface UpdateListener {

	public void onFinishedUpdating();
}
