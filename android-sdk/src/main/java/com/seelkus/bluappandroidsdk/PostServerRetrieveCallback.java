package com.seelkus.bluappandroidsdk;

public interface PostServerRetrieveCallback<T> {
	
	public void onFinished(T param);

}
