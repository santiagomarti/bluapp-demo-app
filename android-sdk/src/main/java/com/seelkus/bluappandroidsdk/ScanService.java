package com.seelkus.bluappandroidsdk;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;


public class ScanService extends Service {

    static boolean managerStarted = false;
    @Override
    public void onCreate() {
        super.onCreate();
        Log.e("BluApp Service", "Service created");
        if(!managerStarted){
            BluManager manager = new BluManager(getApplication());
            manager.start();
        }
        BluManager.initializeService(getApplicationContext());
        Log.e("BluApp Service", "Manager started from service");
    }

	@Override
	public IBinder onBind(Intent arg0) {	
		return null;
	}
	
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		return Service.START_STICKY;
	}
	
	public ScanService(String name) {
		super();
	}
	
	public ScanService() {
		super();
	}
}
