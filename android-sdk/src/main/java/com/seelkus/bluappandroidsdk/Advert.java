package com.seelkus.bluappandroidsdk;

import java.sql.Timestamp;
import java.util.AbstractMap;
import java.util.AbstractMap.SimpleEntry;
import java.util.LinkedList;

import android.annotation.TargetApi;
import android.os.Build;

@TargetApi(Build.VERSION_CODES.GINGERBREAD)
public class Advert extends JSONParseable<Advert> {

	public int AD_TYPE = 0;
	public static final int VIDEO = 1;
	public static final int HTML_CONTENT = 2;
	public static final int IMAGE = 3;
	public static final int URL = 4;
	public static final int NOTIFICATION_TEXT = 5;
	protected boolean wasSent = false, wasOnRange = false, wasSentOnceOnExit = false;
	protected static LinkedList<Integer> supportedTypes;
	public LinkedList<AbstractMap.SimpleEntry<String, Integer>> content;
	
    protected int id,ibeacon_id, active, promote, ad_type, ttl_enter, ttl_exit;
    protected String title, body, cover, url, description, msg_exit, msg_enter;
    protected Timestamp created_at, updated_at;
    protected long lastTimeShowed, firstTimeSeen, lastTimeShowedOnExit;
    protected double lastSeenDistance;
    protected BABeacon beacon;

	/**
	 * Clase que contiene la información relacionada con un advert. Cada advert 
	 * puede ser ingresado a través de la página web del servicio. El contenido 
	 * de cada advert se encuentra en el campo content, que almacena una lista
	 * con valores pares de Strings y Id's de tipo. Los ids de tipo se pueden encontrar
	 * llamando estáticamente a ésta clase. Los strings, dependiendo del tipo, contienen
	 * una url (páginas, vídeos e imágenes), un texto (notificación) o contenido HTML.
	 */
	public Advert()
	{
		supportedTypes = new LinkedList<Integer>();
		supportedTypes.add(VIDEO);
		supportedTypes.add(HTML_CONTENT);
		supportedTypes.add(IMAGE);
		supportedTypes.add(URL);
		supportedTypes.add(NOTIFICATION_TEXT);
	}
	@Override
	protected void setProperFields(String... strings) {
		// TODO Auto-generated method stub
		
	}	
	
	public static Advert cloneRelevantStuff(Advert nuevo, Advert old)
	{
		old.setTitle(nuevo.getTitle());
		old.setActive(nuevo.getActive());
		old.setAd_type(nuevo.getAd_type());
		old.setBody(nuevo.getBody());
		old.setCover(nuevo.getCover());
		old.setDescription(nuevo.getDescription());
		old.setMsg_enter(nuevo.getMsg_enter());
		old.setMsg_exit(nuevo.getMsg_exit());
		old.setPromote(nuevo.getPromote());
		old.setTtl_enter(nuevo.getTtl_enter());
		old.setTtl_exit(nuevo.getTtl_exit());
		old.setUrl(nuevo.getUrl());
		old.setBeacon(nuevo.getBeacon());
		return old;
	}
	public void addContent(String contentSt, Integer type) throws Exception
	{
		if(supportedTypes.contains((Integer)type) == false){
			throw new Exception("Type not supported");
		} else {
			content.add(new SimpleEntry<>(contentSt, type));
		}
	}
	public LinkedList<AbstractMap.SimpleEntry<String, Integer>> getContent() {
		return content;
	}
	public void setContent(
			LinkedList<AbstractMap.SimpleEntry<String, Integer>> content) {
		this.content = content;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getIbeacon_id() {
		return ibeacon_id;
	}
	public void setIbeacon_id(int ibeacon_id) {
		this.ibeacon_id = ibeacon_id;
	}
	public int getActive() {
		return active;
	}
	public void setActive(int active) {
		this.active = active;
	}
	public int getPromote() {
		return promote;
	}
	public void setPromote(int promote) {
		this.promote = promote;
	}
	public String getMsg_enter() {
		return msg_enter;
	}
	public void setMsg_enter(String msg_enter) {
		this.msg_enter = msg_enter;
	}
	public String getMsg_exit() {
		return msg_exit;
	}
	public void setMsg_exit(String msg_exit) {
		this.msg_exit = msg_exit;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getCover() {
		return cover;
	}
	public void setCover(String cover) {
		this.cover = cover;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String cover_url) {
		this.url = cover_url;
	}
	public int getAd_type() {
		return ad_type;
	}
	public void setAd_type(int ad_type) {
		switch(ad_type){
		case IMAGE:
			AD_TYPE = IMAGE;
			break;
		case HTML_CONTENT:
			AD_TYPE = HTML_CONTENT;
			break;
		case VIDEO:
			AD_TYPE = VIDEO;
			break;
		case URL:
			AD_TYPE = URL;
			break;
		default:
			break;
		}
		this.ad_type = ad_type;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Timestamp getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Timestamp created_at) {
		this.created_at = created_at;
	}
	public int getTtl_enter() {
		return ttl_enter;
	}
	public void setTtl_enter(int ttl_enter) {
		this.ttl_enter = ttl_enter;
	}
	public int getTtl_exit() {
		return ttl_exit;
	}
	public void setTtl_exit(int ttl_exit) {
		this.ttl_exit = ttl_exit;
	}
	public Timestamp getUpdated_at() {
		return updated_at;
	}
	public void setUpdated_at(Timestamp updated_at) {
		this.updated_at = updated_at;
	}
	public boolean isWasSent() {
		return wasSent;
	}
	public long getLastTimeShowed() {
		return lastTimeShowed;
	}
	public void setLastTimeShowed(long lastTimeShowed) {
		this.lastTimeShowed = lastTimeShowed;
		DBManager mgr = new DBManager(BluManager.application);
		mgr.updateAdvert(this);
	}
	public BABeacon getBeacon() {
		return beacon;
	}
	public void setBeacon(BABeacon beacon) {
		this.beacon = beacon;
	}
	public long getFirstTimeSeen() {
		return firstTimeSeen;
	}
	public void setFirstTimeSeen(long firstTimeSeen) {
		this.firstTimeSeen = firstTimeSeen;
	}
	public double getLastSeenDistance() {
		return lastSeenDistance;
	}
	public void setLastSeenDistance(double d) {
		this.lastSeenDistance = d;
	}
	public boolean getWasOnRange() {
		return wasOnRange;
	}
	public void setWasOnRange(boolean wasOnRange) {
		this.wasOnRange = wasOnRange;
	}
	public boolean isWasSetOnceOnExit() {
		return wasSentOnceOnExit;
	}
	public void setWasSetOnceOnExit(boolean wasSetOnceOnExit) {
		this.wasSentOnceOnExit = wasSetOnceOnExit;
	}
	public long getLastTimeShowedOnExit() {
		return lastTimeShowedOnExit;
	}
	public void setLastTimeShowedOnExit(long lastTimeShowedOnExit) {
		this.lastTimeShowedOnExit = lastTimeShowedOnExit;
		DBManager mgr = new DBManager(BluManager.application);
		mgr.updateAdvert(this);
	}
}
