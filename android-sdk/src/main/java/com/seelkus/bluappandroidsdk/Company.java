package com.seelkus.bluappandroidsdk;

import android.util.SparseArray;

public class Company {

	private int id, active;
	private String cover, name, description;
	private boolean display;
	private SparseArray<Advert> ads;
	public int getId() {
		return id;
	}
	protected void setId(int id) {
		this.id = id;
	}
	public int getActive() {
		return active;
	}
	protected void setActive(int active) {
		this.active = active;
	}
	public String getCover() {
		return cover;
	}
	protected void setCover(String cover) {
		this.cover = cover;
	}
	public String getName() {
		return name;
	}
	protected void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	protected void setDescription(String description) {
		this.description = description;
	}
	public boolean isDisplay() {
		return display;
	}
	protected void setDisplay(boolean display) {
		this.display = display;
	}
	public SparseArray<Advert> getAds() {
		return ads;
	}
	public void setAds(SparseArray<Advert> ads) {
		this.ads = ads;
	}
	
	
}
