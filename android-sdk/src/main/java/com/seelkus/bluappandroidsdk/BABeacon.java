package com.seelkus.bluappandroidsdk;

import android.util.SparseArray;


public class BABeacon extends JSONParseable<BABeacon>{

	private int id, localId, enter, exit, position, active;
	private String peripheralID;
	private String proximityUUID;
	private long minor, major, rssi;
	private String proximity, description, name, identifier;
	private SparseArray<Advert> ads;
	private Local local;
	private long timein = 10, timeout = 10;
	private long lastTimeSpotted = 0L;
	private boolean onRange, dataTrackerOnRange = false;
	private double oneOldDistance = -1;
	private double twoOldDistance = -1;
	public BABeacon() {}
	
	@Override
	protected void setProperFields(String... strings) {
		// TODO Auto-generated method stub
		
	}
	
	public int getId(){
		return id;
	}
	protected void setId(int id){
		this.id = id;
	}
	public int getLocalId() {
		return localId;
	}
	protected void setLocalId(int localId) {
		this.localId = localId;
	}
	public int getEnter() {
		return enter;
	}
	protected void setEnter(int enter) {
		this.enter = enter;
	}
	public int getExit() {
		return exit;
	}
	protected void setExit(int exit) {
		this.exit = exit;
	}
	public String getPeripheralID() {
		return peripheralID;
	}
	protected void setPeripheralID(String peripheralID) {
		this.peripheralID = peripheralID;
	}
	public String getProximityUUID() {
		return proximityUUID;
	}
	protected void setProximityUUID(String proximityUUID) {
		this.proximityUUID = proximityUUID;
	}
	public long getMinor() {
		return minor;
	}
	protected void setMinor(long minor) {
		this.minor = minor;
	}
	public long getMajor() {
		return major;
	}
	protected void setMajor(long mayor) {
		this.major = mayor;
	}
	public long getRssi() {
		return rssi;
	}
	protected void setRssi(long rssi) {
		this.rssi = rssi;
	}
	public String getProximity() {
		return proximity;
	}
	protected void setProximity(String proximity) {
		this.proximity = proximity;
	}
	public String getDescription() {
		return description;
	}
	protected void setDescription(String description) {
		this.description = description;
	}
	public SparseArray<Advert> getAds() {
		return ads;
	}
	protected void setAds(SparseArray<Advert> ads) {
		this.ads = ads;
	}
	public Local getLocal() {
		return local;
	}
	protected void setLocal(Local local) {
		this.local = local;
	}

	public String getName() {
		return name;
	}

	protected void setName(String name) {
		this.name = name;
	}

	public int getPosition() {
		return position;
	}

	protected void setPosition(int position) {
		this.position = position;
	}

	public String getIdentifier() {
		return identifier;
	}

	protected void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public long getTimein() {
		return timein;
	}

	protected void setTimein(long timein) {
		this.timein = timein;
	}

	public long getTimeout() {
		return timeout;
	}

	protected void setTimeout(long timeout) {
		this.timeout = timeout;
	}

	public int getActive() {
		return active;
	}

	protected void setActive(int active) {
		this.active = active;
	}	
	
	public long getLastTimeSpotted() {
		return lastTimeSpotted;
	}
	/**
	 * Método público, mejor que sea privado
	 */
	protected void setLastTimeSpotted(long lastTimeSpotted) {
		this.lastTimeSpotted = lastTimeSpotted;
	}

	public boolean isOnRange() {
		return onRange;
	}
	/**
	 * Método público, mejor que sea privado
	 * @param onRange
	 */
	protected void setOnRange(boolean onRange) {
		this.onRange = onRange;
	}

	public boolean isDataTrackerOnRange() {
		return dataTrackerOnRange;
	}

	public void setDataTrackerOnRange(boolean dataTrackerOnRange) {
		this.dataTrackerOnRange = dataTrackerOnRange;
	}

	public double getDistance(double newDistance){
		if(oneOldDistance == -1){
			oneOldDistance = newDistance;
			return  newDistance;
		} else {
			twoOldDistance = oneOldDistance;
			oneOldDistance = newDistance;
			return oneOldDistance + twoOldDistance / 2;
		}
	}

    public double getDistance(){
        if(oneOldDistance == -1){
            return  -1;
        } else if (oneOldDistance != -1 && twoOldDistance == -1) {
            return oneOldDistance;
        } else return oneOldDistance + twoOldDistance / 2;
    }
}

/**
 * oneOld en -1, twoOld en -1 -> oneOld a newDistance, ret newDistance
 * oneOld en 12312, twoOld en -1 -> oneOld a twoOld, newDistance a oneOld, ret one + two / 2
 * oneOld en 1322, twoOld en 1234 -> newDistance a oneOld, oneOld a twoOld, ret one + two / 2
 */
