package com.seelkus.bluappandroidsdk;

public class BluetoothNotSupportedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public BluetoothNotSupportedException(String message)
	{
		super(message);
	}

}
