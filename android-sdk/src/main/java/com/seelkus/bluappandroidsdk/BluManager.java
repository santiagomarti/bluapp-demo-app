package com.seelkus.bluappandroidsdk;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;

import org.altbeacon.beacon.Beacon;
import org.altbeacon.beacon.BeaconConsumer;
import org.altbeacon.beacon.BeaconManager;
import org.altbeacon.beacon.BeaconParser;
import org.altbeacon.beacon.RangeNotifier;
import org.altbeacon.beacon.Region;
import org.altbeacon.beacon.service.RunningAverageRssiFilter;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import android.app.Application;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.util.SparseArray;

/**
 * Clase base
 * 
 * @author Santiago Martí Olbrich
 * mns01.domaincontrol.com
 *
 */
public class BluManager {
	
	protected static Application application;
	protected static SparseArray<BABeacon> onRangeBeacons = new SparseArray<BABeacon>();
	protected static SparseArray<String> notificationTitles = new SparseArray<>();
	private static boolean SCAN = true;	
	private static boolean checkForBeaconRanging = false;

	private static BeaconManager beaconManager;
	private static BluManager instance;	
	private static LinkedList<BeaconsStateListener> beaconListeners = null;
	private static HashMap<Integer, Advert> localAds = new HashMap<Integer, Advert>();
	private static HashMap<Beacon, BABeacon> downloadedBeacons = new HashMap<>();
	private static LinkedList<BABeacon> clientBeacons = null;
	private static String clientEmail = "";
	protected static String clientCompany = "";
    private static int appLogoId;
	private static int smallLogoId;
    private static String appLogoSt, smallLogoSt;
	private DBManager dbManager;
	private static final String client = "bluapp-client";
	private static final String company = "bluapp-company";
	private static final String smallicon = "bluapp-smallicon";
	private static final String largeicon = "bluapp-largeicon";
    private static int trackCounter = 0;
    private static final int TRACK_LIMIT = 10;

	/** Base constructor used to initialize the basic functionalities, which are, push notifications and
	 * showing its related content.
	 *
	 * @param application
	 */
	public BluManager(Application application){
		try {
			ApplicationInfo ai = application.getPackageManager().getApplicationInfo(application.getPackageName(), PackageManager.GET_META_DATA);
			Bundle bundle = ai.metaData;
			BluManager.appLogoSt = bundle.getString(largeicon);
			BluManager.clientCompany = bundle.getString(company);
			BluManager.smallLogoSt = bundle.getString(smallicon);
            BluManager.application = application;
            if(!mailOnSharedPreferences()) {
                BluManager.clientEmail = bundle.getString(client);
            }
            else{
                SharedPreferences sharedPref = application.getSharedPreferences("bluapp", Context.MODE_PRIVATE);
                BluManager.clientEmail = sharedPref.getString("mail", "");
            }
            appLogoId = application.getResources().getIdentifier(appLogoSt, "drawable", application.getPackageName());
            smallLogoId = application.getResources().getIdentifier(smallLogoSt, "drawable", application.getPackageName());
		} catch (PackageManager.NameNotFoundException e){
			Log.e("Bluapp", "BluManager could not be initialized. Missing metadata?");
		}
	}

    private boolean mailOnSharedPreferences() {
        return !application.getSharedPreferences("bluapp", Context.MODE_PRIVATE).getString("mail", "").equals("");
    }

    public void start() {
        if(!ScanService.managerStarted){
            beaconManager = BeaconManager.getInstanceForApplication(application);
            beaconManager.getBeaconParsers().add(new BeaconParser().setBeaconLayout("m:2-3=0215,i:4-19,i:20-21,i:22-23,p:24-24,d:25-25"));
            BeaconManager.setRssiFilterImplClass(RunningAverageRssiFilter.class);
            RunningAverageRssiFilter.setSampleExpirationMilliseconds(5000l);
            //BeaconManager.setRssiFilterImplClass(ArmaRssiFilter.class);
            if(BluManager.clientEmail != null){
                ScanService.managerStarted = true;
                application.startService(new Intent(application, ScanService.class));
                Log.v("Bluapp", "BluManager started");
            }
            beaconListeners = new LinkedList<>();
            IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
            application.registerReceiver(mReceiver, filter);
            instance = this;
            dbManager = new DBManager(application);
        }
	}

	private static double calculateAccuracy(int txPower, double rssi, BABeacon beacon) {
		if (rssi == 0) {
			return -1.0; // if we cannot determine accuracy, return -1.
		}
		double acc = 0;
		double ratio = rssi * 1.0 / txPower;
		if (ratio < 1.0) {
			return beacon.getDistance(Math.pow(ratio, 10));
		} else {
			acc = (0.89976) * Math.pow(ratio, 7.7095) + 0.111;
            return beacon.getDistance(acc);
		}
	}

	private static boolean belongsToCompany(int id) {
		try {
			if(clientBeacons == null){
				clientBeacons = WebServiceClient.getClientBeacons();
			}			
			for (BABeacon b : clientBeacons)
				if (id == b.getId())
					return true;
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return false;
	}

    private static boolean belongsToCompany(int major, int minor) {
        try {
            if(clientBeacons == null){
                clientBeacons = WebServiceClient.getClientBeacons();
            }
            for (BABeacon b : clientBeacons)
                if (major == b.getMajor() && minor == b.getMinor())
                    return true;
        } catch (ClientProtocolException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

	private static BABeacon getServerBeacon(Beacon b) {
		if(downloadedBeacons.containsKey(b)){
			return downloadedBeacons.get(b);
		}
		try {
			BABeacon aux = WebServiceClient.getBeacon("f7826da6-4fa2-4e98-8024-bc5b71e0893e", b.getId2().toInt(), b.getId3().toInt());
			downloadedBeacons.put(b, aux);
			return aux;
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new BABeacon();
	}

	private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			final String action = intent.getAction();

			if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
				final int state = intent.getIntExtra(
						BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
				switch (state) {
				case BluetoothAdapter.STATE_OFF:
					break;
				case BluetoothAdapter.STATE_TURNING_OFF:
                    DataTracker.onBluetoothOff();
                    onRangeBeacons = new SparseArray<>();
					SCAN = false;
					break;
				case BluetoothAdapter.STATE_ON:
					break;
				case BluetoothAdapter.STATE_TURNING_ON:
					SCAN = true;
					break;
				}
			}
		}
	};

	protected static void onAdvertFoundProcess(BABeacon b, Beacon beacon) {
		for (int i = 0; i < b.getAds().size(); i++) {
			Advert aux = b.getAds().valueAt(i);
			Advert localAdvert = getLocalAdvert(aux);
			if (localAdvert == null) {
				aux.setLastSeenDistance(calculateAccuracy(beacon.getTxPower(),
						beacon.getRssi(), b));
				localAds.put(aux.getId(), aux);
				if (shouldSendAdvert(aux, beacon, b)) {
					onReceivedNotification(aux, b, entering, exiting);
				}
			} else 
				if (shouldSendAdvert(Advert.cloneRelevantStuff(aux, localAdvert), beacon,b)) 
					onReceivedNotification(aux, b, entering, exiting);
		}
	}

	protected static Advert getLocalAdvert(Advert serverAdvert) {
		return localAds.get(serverAdvert.getId());
	}

	protected static void initializeService(final Context ctx) {
		beaconManager.bind(new BeaconConsumer() {
			
			@Override
			public void unbindService(ServiceConnection arg0) {
				ctx.unbindService(arg0);
			}
			
			@Override
			public void onBeaconServiceConnect() {

				beaconManager.setRangeNotifier(new RangeNotifier() {

					@Override
					public void didRangeBeaconsInRegion(Collection<Beacon> beacons,Region region) {
						if (SCAN && beacons.size() > 0) {
							for (Beacon b : beacons) {
								BABeacon aux = getServerBeacon(b);
								if (aux != null && belongsToCompany(aux.getId())){
                                    setOnRangeInfo(aux);
                                    aux.setLastTimeSpotted(System.currentTimeMillis());
                                    for (int i = 0; i < onRangeBeacons.size(); i++)
                                        if (onRangeBeacons.valueAt(i).getId() == aux.getId()) {
                                            onRangeBeacons.valueAt(i).setLastTimeSpotted(System.currentTimeMillis());
                                            break;
                                        }
                                    aux.setOnRange(true);
                                    onAdvertFoundProcess(aux, b);
                                    if (beaconListeners != null && shouldSendBeacon(aux, b)) {
                                        for (BeaconsStateListener bs : beaconListeners)
                                            bs.onRangedBeacon(aux);
                                    }
                                    if (shouldSendBeacon(aux, b)) {
                                        aux.setDataTrackerOnRange(true);
                                    }
                                }
							}

                            //Sacamos los beacons que salieron de rango
							for (int i = 0; i < onRangeBeacons.size(); i++) {
								BABeacon aux = onRangeBeacons.valueAt(i);
								if (aux.isOnRange() && aux.getDistance() > getDistance(aux.getProximity()) && aux.getActive() == 1) {
									aux.setOnRange(false);
									aux.setLastTimeSpotted(-1);
                                    onRangeBeacons.removeAt(i);
								}
							}
                            //Sacamos los beacons que ya no vemos
                            LinkedList<Integer> toKeep = new LinkedList<>();
                            for(int i = 0; i < onRangeBeacons.size(); i++)  {
                                BABeacon aux = onRangeBeacons.valueAt(i);
                                for(Beacon b : beacons)
                                    if (b.getId2().toInt() == aux.getMajor() && b.getId3().toInt() == aux.getMinor() && belongsToCompany(b.getId2().toInt(), b.getId3().toInt())) {
                                        toKeep.add(aux.getId());
                                        break;
                                    }
                            }
                            LinkedList<Integer> toRemove = new LinkedList<>();
                            for(int i = 0; i < onRangeBeacons.size(); i++)  {
                                boolean present = false;
                                for(Integer j : toKeep)
                                    if (j == onRangeBeacons.valueAt(i).getId()) {
                                        present = true;
                                        break;
                                    }
                                if(present == false)
                                    toRemove.add(i);
                                present = false;
                            }
                            for(Integer k : toRemove){
                                onRangeBeacons.removeAt(k);
                            }
						}

                        trackCounter++;
                        if(trackCounter > TRACK_LIMIT){
                            DataTracker.trackingService();
                            trackCounter = 0;
                        }
					}
				});
                try {
                    beaconManager.startRangingBeaconsInRegion(new Region("myRangingUniqueId", null, null, null));
                } catch (RemoteException e) {
                }
			}
			
			@Override
			public Context getApplicationContext() {
				return ctx;
			}
			
			@Override
			public boolean bindService(Intent arg0, ServiceConnection arg1, int arg2) {
				return ctx.bindService(arg0, arg1, arg2);
			}
		});
	}

	private static boolean entering;
	private static boolean exiting;
	private static long lastTimeNotifSent = 0;

	protected static String getDeviceId(){
		TelephonyManager tm = (TelephonyManager)  application.getSystemService(Context.TELEPHONY_SERVICE);
		return tm.getDeviceId();
	}
	
	private static void onReceivedNotification(Advert add, BABeacon beacon,
			boolean entering, boolean exiting) {
		String url = add.getUrl();
		if(entering) 
			DataTracker.onNotificationSentEnter(add.getId(), beacon, getDeviceId());
		NotificationManager notificationManager = (NotificationManager) application
				.getSystemService(Context.NOTIFICATION_SERVICE);
		Intent intent = null;
		switch (add.AD_TYPE) {
			case Advert.HTML_CONTENT:
                break;
            case Advert.IMAGE:
                intent = new Intent(application,NotificationReceiver.class);
                intent.putExtra("id", add.getId());
                intent.putExtra("url", add.getUrl());
                intent.putExtra("adtype", add.AD_TYPE);
                intent.putExtra("fromnotification", true);
                intent.putExtra("enter", entering);
                intent.putExtra("proxuuid", beacon.getProximityUUID());
                intent.putExtra("minor", beacon.getMinor());
                intent.putExtra("major", beacon.getMajor());
                break;
            case Advert.URL:
                intent = new Intent(application,NotificationReceiver.class);
                intent.putExtra("id", add.getId());
                intent.putExtra("url", url);
                intent.putExtra("adtype", add.AD_TYPE);
                intent.putExtra("fromnotification", true);
                intent.putExtra("enter", entering);
                intent.putExtra("proxuuid", beacon.getProximityUUID());
                intent.putExtra("minor", beacon.getMinor());
                intent.putExtra("major", beacon.getMajor());
                break;
            case Advert.VIDEO:
                intent = new Intent(application,NotificationReceiver.class);
                intent.putExtra("id", add.getId());
                intent.putExtra("url", url);
                intent.putExtra("adtype", add.AD_TYPE);
                intent.putExtra("fromnotification", true);
                intent.putExtra("enter", entering);
                intent.putExtra("proxuuid", beacon.getProximityUUID());
                intent.putExtra("minor", beacon.getMinor());
                intent.putExtra("major", beacon.getMajor());
                break;
            default:
                break;
		}
		notificationTitles.append(add.getId(), (entering ? add.getMsg_enter() : add.getMsg_exit()));
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
		PendingIntent contentIntent = PendingIntent.getActivity(application,(int) System.currentTimeMillis(), intent,PendingIntent.FLAG_CANCEL_CURRENT);
		Notification.Builder mBuilder = null;
		Uri soundUri2 = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		
		mBuilder = new Notification.Builder(application).setSmallIcon(smallLogoId).setContentTitle(add.getTitle()).setContentText((entering ? add.getMsg_enter() : add.getMsg_exit()))
				.setAutoCancel(true).setLights(0xff0000ff, 300, 2000).setLargeIcon(getResizedBitmap(BitmapFactory.decodeResource(application.getResources(), appLogoId),
				(int) application.getResources().getDimension(android.R.dimen.notification_large_icon_height)));
		
		if (System.currentTimeMillis() - lastTimeNotifSent > 2000) {
			mBuilder = mBuilder.setSound(soundUri2);
		}
		
		mBuilder = mBuilder.setContentIntent(contentIntent);
		// Display notification
		notificationManager.notify(add.getId(), mBuilder.build());
		lastTimeNotifSent = System.currentTimeMillis();
	}

	private static boolean shouldSendBeacon(BABeacon babeacon, Beacon beacon) {
		if(babeacon.getActive() == 0) 
			return false;
		return calculateAccuracy(beacon.getTxPower(), beacon.getRssi(), babeacon) < getDistance(babeacon.getProximity());
	}

	private static boolean shouldSendAdvert(Advert ad, Beacon beacon, BABeacon babeacon) {
		if(babeacon.getActive() == 0)
			return false;
		if (ad.getActive() == 0)
			return false;// El add no esta activo
		if (ad.getPromote() == 0)
			return false;// La notificacion no está activa

		if (ad.getTtl_enter() < 1)
			ad.setTtl_enter(10);// Seteamos a minimo 10 minutos.
		if (ad.getTtl_exit() < 1)
			ad.setTtl_exit(10);

		boolean onEnter = (ad.getMsg_enter().equals("") ? false : true);
		//boolean onExit = (ad.getMsg_exit().equals("") ? false : true);

		if (ad.getFirstTimeSeen() == 0) {
			ad.setFirstTimeSeen(System.currentTimeMillis());// Primera vez
		}

		if (onEnter) {
			if (ad.getLastTimeShowed() == 0 && calculateAccuracy(beacon.getTxPower(), beacon.getRssi(), babeacon) < getDistance(ad.getBeacon().getProximity())) {
				ad.setLastTimeShowed(System.currentTimeMillis());
				entering = true;
				exiting = false;
				return true;
			}
			if ((System.currentTimeMillis() - ad.getLastTimeShowed()) / 1000 > ad.getTtl_enter() * 60 * 60 && calculateAccuracy(beacon.getTxPower(), beacon.getRssi(), babeacon) < getDistance(ad.getBeacon().getProximity())) {
				ad.setLastTimeShowed(System.currentTimeMillis());
				entering = true;
				exiting = false;
				return true;
			}
		} else return false;
		return false;
	}

	private static double getDistance(String proximity) {
		if (proximity.equals("immediate"))
			return 2;
		else if (proximity.equals("near"))
			return 5;
		else if (proximity.equals("far"))
			return 10;
		else
			return 100;
	}

	/**
	 * Metodo que actualiza la info que esta en rango, es decir, los beacons,
	 * ads y compa�ias que deben ser mostrados. No debe estar en la libreria
	 * final, ya que esta solo deberia mostrar los adverts de UNA sola compa�ia,
	 * no de todas.
	 * 
	 * @param b
	 */
	private static void setOnRangeInfo(BABeacon b) {
		if (onRangeBeacons.get(b.getId(), null) == null) {
			onRangeBeacons.append(b.getId(), b);
		}
	}

	public void registerBeaconStateListener(BeaconsStateListener listener) {
		if (beaconListeners.contains(listener))
			return;
		else
			beaconListeners.add(listener);
	}

	private static Bitmap getResizedBitmap(Bitmap image, int maxSize) {
		if (image == null)
			return image;
		int width = image.getWidth();
		int height = image.getHeight();

		float bitmapRatio = (float) width / (float) height;
		if (bitmapRatio > 0) {
			width = maxSize;
			height = (int) (width / bitmapRatio);
		} else {
			height = maxSize;
			width = (int) (height * bitmapRatio);
		}
		return Bitmap.createScaledBitmap(image, width, height, true);
	}

	protected static String getClientEmail() {
		return clientEmail;
	}

    private class Tuple<X, Y> {
        public final X x;
        public final Y y;
        public Tuple(X x, Y y) {
            this.x = x;
            this.y = y;
        }
    }
}
