package com.seelkus.bluappandroidsdk;

import java.io.InputStream;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.widget.ImageView;

public class ImageViewActivity extends Activity {

	private String img_url;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().requestFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_image_view);

        String img_url = "http://54.233.70.75/contentimages/" + getIntent().getStringExtra("url").replace(" ", "%20");
		
		BitmapFactory.Options options = new BitmapFactory.Options();
		options.inSampleSize = 2;
	
		ZoomFunctionality img = new ZoomFunctionality(this);
		new DownloadImageTask(img).execute(img_url);
		img.setMaxZoom(4f);
		setContentView(img);
	}

	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
	    ImageView bmImage;
	    AlertDialog.Builder dialog = new Builder(ImageViewActivity.this);
	    AlertDialog alert = null;
	    
	    public DownloadImageTask(ImageView bmImage) {
	        this.bmImage = bmImage;
	    }

	    @Override
	    protected void onPreExecute() {
	    	super.onPreExecute();
	    	alert = dialog.setTitle(BluManager.clientCompany).setMessage("Cargando imagen...").setCancelable(false).create();
	    	alert.show();
	    }
	    
	    protected Bitmap doInBackground(String... urls) {
	        String urldisplay = urls[0];
	        Bitmap mIcon11 = null;
	        try {
	            InputStream in = new java.net.URL(urldisplay).openStream();
	            mIcon11 = BitmapFactory.decodeStream(in);
	        } catch (Exception e) {
	            Log.e("Error", e.getMessage());
	            e.printStackTrace();
	        }
	        return mIcon11;
	    }

	    protected void onPostExecute(Bitmap result) {
		        bmImage.setImageBitmap(result);
		        alert.dismiss();
	    }
	}
}
