package com.seelkus.bluappandroidsdk;

/**
 * Clase abstracta que define comportamiento para aquellos objetos que 
 * deben ser parseados desde un string Json y que vienen desde el servidor
 * @author smarti
 *
 * @param <T>
 */
public abstract class JSONParseable<T>{
	
	protected boolean properFieldsSetted = false;
	
	protected abstract void setProperFields(String...strings );
	
	protected T parseFromJsonObject()
	{
		return null;
		
	}
	

}
