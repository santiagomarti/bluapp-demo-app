package com.seelkus.bluappandroidsdk;

import java.io.IOException;
import java.util.LinkedList;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import android.util.SparseArray;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;

public class JSONParser {

	
	public JsonArray getNestedArray(JsonElement beacon, String arrayName){
		return beacon.getAsJsonObject().getAsJsonArray(arrayName);
	}
	
	public LinkedList<BABeacon> parseBeacons(String result){
		JsonParser parser = new JsonParser();
		LinkedList<BABeacon> ret = new LinkedList<BABeacon>();
		JsonArray beacons = parser.parse(result).getAsJsonObject().getAsJsonArray("response");
		for(JsonElement obj : beacons){
			ret.add(parseBeacon(obj, false, false));
		}
		return ret;
	}
	
	public BABeacon parseBeacon(JsonElement result, boolean onArray, boolean completeInfoNeeded){
		JsonElement beacon;
		if(onArray){
			if(result.getAsJsonObject().getAsJsonArray("response").size() > 0)
				beacon = result.getAsJsonObject().getAsJsonArray("response").get(0);
			else return null;
		} else beacon = result;
		BABeacon ret = new BABeacon();
        JsonObject beaconObj = beacon.getAsJsonObject();
		ret.setId(beaconObj.getAsJsonPrimitive("id").getAsInt());
        ret.setMinor(beaconObj.getAsJsonPrimitive("minor").getAsLong());
        ret.setMajor(beaconObj.getAsJsonPrimitive("major").getAsLong());
		//ret.setDescription(beacon.getAsJsonObject().getAsJsonPrimitive("description").getAsString());
        if (completeInfoNeeded){
			ret.setEnter(beaconObj.getAsJsonPrimitive("enter").getAsInt());
            ret.setExit(beaconObj.getAsJsonPrimitive("exit").getAsInt());
			//ret.setLocalId(beacon.getAsJsonObject().getAsJsonPrimitive("local_id").getAsInt());
			ret.setPeripheralID(beaconObj.getAsJsonPrimitive("peripheralid").getAsString());
			ret.setProximity(beaconObj.getAsJsonPrimitive("proximity").getAsString());
			ret.setProximityUUID(beaconObj.getAsJsonPrimitive("proximityuuid").getAsString());
			ret.setRssi(beaconObj.getAsJsonPrimitive("rssi").getAsInt());
			//ret.setName(beaconObj.getAsJsonPrimitive("name").getAsString());
			//TODO Verificar timestamps ret.setTimein(null);
			//TODO Verificar timestamps ret.setTimeout(null);
			ret.setActive(beaconObj.getAsJsonPrimitive("active").getAsInt());
			//ret.setIdentifierbeaconObj.getAsJsonPrimitive("identifier").getAsString());
			//ret.setLocalId(Integer.parseInt(beaconObj.getAsJsonPrimitive("position").getAsString()));
		}	
		ret.setAds(new SparseArray<Advert>());
		if(completeInfoNeeded)
			try {
				LinkedList<Advert> aux = WebServiceClient.getCampaign(ret.getId());
				for(Advert ad : aux){
					ret.getAds().append(ad.getId(), ad);
					ad.setBeacon(ret);
				}
			} catch (ClientProtocolException e) {e.printStackTrace();
			} catch (JSONException e) {e.printStackTrace();
			} catch (IOException e) {e.printStackTrace();}
	
		//ret.setLocal(parseLocal(beacon.getAsJsonObject().get("local")));
		return ret;
	}
	
	public LinkedList<Advert> parseAdverts(JsonElement result){
		LinkedList<Advert> rets = new LinkedList<Advert>();
		for(int i = 0 ; i < result.getAsJsonObject().getAsJsonArray("response").size() ; i++){
            JsonObject advert = result.getAsJsonObject().getAsJsonArray("response").get(i).getAsJsonObject();
			Advert ret = new Advert();
			//ret.setDescription(advert.getAsJsonObject().getAsJsonPrimitive("body").getAsString());
			ret.setActive(advert.getAsJsonPrimitive("active").getAsInt());
			ret.setTtl_enter(advert.getAsJsonPrimitive("ttl_enter").getAsInt());
			ret.setTtl_exit(advert.getAsJsonPrimitive("ttl_exit").getAsInt());
			String type = advert.getAsJsonPrimitive("type").getAsString();
			if(type.equals("image")){ ret.setAd_type(Advert.IMAGE);}
			else if(type.equals("html-embeded")){ ret.setAd_type(Advert.HTML_CONTENT);}
			else if(type.equals("video")){ ret.setAd_type(Advert.VIDEO);}
			else if(type.equals("html-url")){ ret.setAd_type(Advert.URL);}
//			ret.setBody(advert.getAsJsonObject().getAsJsonPrimitive("body").getAsString());
			if(advert.get("cover").isJsonNull() == false)
				ret.setCover(advert.getAsJsonPrimitive("cover").getAsString());
			else ret.setCover("");
			if(advert.get("url").isJsonNull() == false)
				ret.setUrl(advert.getAsJsonPrimitive("url").getAsString());
            else ret.setUrl("");
			ret.setIbeacon_id(advert.getAsJsonPrimitive("ibeacons_id").getAsInt());
			ret.setId(advert.getAsJsonPrimitive("id").getAsInt());
            if(advert.get("msg_enter").isJsonNull() == false)
			    ret.setMsg_enter(advert.getAsJsonPrimitive("msg_enter").getAsString());
            else ret.setMsg_enter("");
            if(advert.get("msg_exit").isJsonNull() == false)
			    ret.setMsg_exit(advert.getAsJsonPrimitive("msg_exit").getAsString());
            else ret.setMsg_exit("");
			ret.setPromote(advert.getAsJsonPrimitive("promote").getAsInt());
			ret.setTitle(advert.getAsJsonPrimitive("title").getAsString());
			//TODO Verificar timestamps ret.setCreated_at(advert.getAsJsonObject().getAsJsonPrimitive("proximity").getAsString());
			//TODO Verificar timestamps ret.setUpdated_at();
            //if(ret.getCover().equals("eliminado") == false)
			DBManager manager = new DBManager(BluManager.application);
			if(manager.advertExistsOnDB(ret.getId())){
				Advert aux = manager.getAdvert(ret.getId());
				ret.setLastTimeShowedOnExit(aux.getLastTimeShowedOnExit());
				ret.setLastTimeShowed(aux.getLastTimeShowed());
				ret.setFirstTimeSeen(aux.getFirstTimeSeen());
			} else {
				manager.createAdvert(ret);
			}
			rets.add(ret);
		}	
		return rets;
	}

	public Company parseCompany(JsonElement result){
        JsonObject company = result.getAsJsonObject().getAsJsonObject("response").getAsJsonObject();

		Company ret = new Company();
		ret.setDescription(company.getAsJsonPrimitive("description").getAsString());
		ret.setActive(company.getAsJsonPrimitive("active").getAsInt());
		ret.setCover(company.getAsJsonPrimitive("cover").getAsString());
		ret.setId(company.getAsJsonPrimitive("id").getAsInt());
		ret.setName(company.getAsJsonPrimitive("name").getAsString());
		//TODO Not valid integer ret.setDisplay(company.getAsJsonObject().getAsJsonPrimitive("display").getAsBoolean());
		
		if(company.getAsJsonObject().getAsJsonArray("ads") != null){
			ret.setAds(new SparseArray<Advert>());
			for(JsonElement ad : getNestedArray(company, "ads")){
				LinkedList<Advert> aux = parseAdverts(ad);
				for(Advert ad2 : aux){
					ret.getAds().append(ad2.getId(), ad2);
				}		
			}	
		}
		return ret;
	}
}
