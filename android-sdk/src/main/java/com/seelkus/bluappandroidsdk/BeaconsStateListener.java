package com.seelkus.bluappandroidsdk;

public interface BeaconsStateListener {

	public void onRangedBeacon(BABeacon param);
	public void onUnrangedBeacon(BABeacon param);
}
